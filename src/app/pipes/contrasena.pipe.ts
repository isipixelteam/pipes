import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'contrasena'
})
export class ContrasenaPipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
  
    if( args[0] ){
      let cadena:string = "";
      for(let l in value){
        cadena += '*';
      }
      return cadena;
    } else {
      return value;
    }
  }

}
